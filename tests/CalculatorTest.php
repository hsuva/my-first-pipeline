<?php

use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase {
    // test the Calculator::add function
    public function testAdd () {
        print_r($_SERVER['BAM_ENV'] . '_test');
        print_r($_SERVER['ACCT_VAR'] . '_test');
        print_r($_SERVER['REPO'] . '_test');
        print_r($_SERVER['PROD_VAR'] . '_test');
        print_r($_SERVER['PROD_VAR2'] . '_test');
        echo env('ACCT_VAR');
        
        $this->assertEquals(1, Calculator::add(0, 1));
    }

    // test the Calculator::subtract function
    public function testSubtract () {
        $this->assertEquals(2, Calculator::subtract(4, 2));
    }

    // test the Calculator::multiply function
    public function testMultiply () {
        $this->assertEquals(3, Calculator::multiply(3, 1));
    }

    // test the Calculator::divide function
    public function testDivide () {
        $this->assertEquals(4, Calculator::divide(16, 4));
    }
}
